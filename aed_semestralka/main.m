% clc, clear, close all

%% Data loading and fragmentation
tab = readtable('Data.xls');
age = table2array(tab(1:140,1));
meas = table2array(tab(1:140,6:37));   

Vavgs = [meas(:,2),meas(:,6),meas(:,10),meas(:,14)]; %horizontal left, right, vertical up, down
Vmaxs = [meas(:,3),meas(:,7),meas(:,11),meas(:,15)]; %horizontal left, right, vertical up, down
Vlatencys = [meas(:,1),meas(:,5),meas(:,9),meas(:,13),meas(:,17),...
    meas(:,19),meas(:,21),meas(:,23),meas(:,25),meas(:,27),meas(:,29),...
    meas(:,31)];
Verrs = [meas(:,18), meas(:,20), meas(:,22), meas(:,24), meas(:,26),...
    meas(:,28), meas(:,30), meas(:,32)];
Vgains = [meas(:,4), meas(:,8), meas(:,12), meas(:,16)];

%% Normality test (All measured variable)
normal_mat = zeros(1,length(meas(1,:)));
for i = 1:length(normal_mat)
    normal_mat(i) = kstest((meas(:,i) - mean(meas(:,i)))/std(meas(:,i)));
end
names = tab.Properties.VariableNames;
names = names(6:end);
normal = table((names(normal_mat==0)),(names(normal_mat==1)));
% Output table (e.g. show normal dist.: normal.Normal)
normal.Properties.VariableNames = {'Normal','Non_normal'};

%% Show distributions of measured data with fitted gaussian curve
lats_means = mean(Vlatencys);
lats_std = std(Vlatencys);
x = 0:.1:600;
figure
for i = 1:length(lats_means)
    gauss = normpdf(x,lats_means(i),lats_std(i));
    plot(x,gauss), hold on
end
hold off

%% Correlations between latencys and age, Pearson
[corr_lat_age(:,1), corr_lat_age(:,2)] = corr(age, Vlatencys, 'type', 'Pearson');
% sprintf('Correraliton between latencys and age\n\n \tFirst collum:  linear correlation coefficients\n \tSecond collum: p-values for testing the hypothesis of no correlation against the alternative that there is a nonzero correlation\n')
% disp(corr_lat_age)
tab_lat_age = table(corr_lat_age(:,1),corr_lat_age(:,2));
tab_lat_age.Properties.VariableNames = {'corr_koef','p_value'};
tab_lat_age.Properties.RowNames = {'p_latency_r','p_latency_l',...
    'p_latency_u','p_latency_d','a_latency_r','a_latency_l','a_latency_u'...
    'a_latency_d','m_latency_r','m_latency_l','m_latency_u','m_latency_d'};
%% Correlations between prosaccade avg speeds and age, Pearson
[corr_Vavg_age(:,1), corr_Vavg_age(:,2)] = corr(age, Vavgs, 'type', 'Pearson');
% sprintf('Correraliton between prosaccade average speeds and age \n\n \tFirst collum:  linear correlation coefficients\n \tSecond collum: p-values for testing the hypothesis of no correlation against the alternative that there is a nonzero correlation\n')
% disp(corr_Vavg_age)
tab_avg_age = table(corr_Vavg_age(:,1),corr_Vavg_age(:,2));
tab_avg_age.Properties.VariableNames = {'corr_koef','p_value'};
tab_avg_age.Properties.RowNames = {'p_av_speed_r', 'p_av_speed_l'...
    'p_av_speed_u', 'p_av_speed_d', };

%% Correlations between max speeds avg speeds and age, Pearson
[corr_Vmax_age(:,1), corr_Vmax_age(:,2)] = corr(age, Vmaxs, 'type', 'Pearson');
% sprintf('Correraliton between prosaccade maximum speeds and age \n\n \tFirst collum:  linear correlation coefficients\n \tSecond collum: p-values for testing the hypothesis of no correlation against the alternative that there is a nonzero correlation\n')
% disp(corr_Vmax_age)
tab_max_age = table(corr_Vmax_age(:,1),corr_Vmax_age(:,2));
tab_max_age.Properties.VariableNames = {'corr_koef','p_value'};
tab_max_age.Properties.RowNames = {'p_max_speed_r', 'p_max_speed_l'...
    'p_max_speed_u', 'p_max_speed_d'};

%% Corelations between age and latency errs, Spearman
[corr_Verr_age(:,1), corr_Verr_age(:,2)] = corr(age, Verrs, 'type', 'Spearman');
% sprintf('Correraliton between errs and age \n\n \tFirst collum:  linear correlation coefficients\n \tSecond collum: p-values for testing the hypothesis of no correlation against the alternative that there is a nonzero correlation\n')
% disp(corr_Verrs_age)
tab_err_age = table(corr_Verr_age(:,1),corr_Verr_age(:,2));
tab_err_age.Properties.VariableNames = {'corr_koef','p_value'};
tab_err_age.Properties.RowNames = {'a_latency_r_err', 'a_latency_l_err'...
    'a_latency_u_err', 'a_latency_d_err', 'm_latency_r_err',...
    'm_latency_l_err', 'm_latency_u_err', 'm_latency_d_err'};

%% Corelations between gain and age, Pearson
[corr_Vgain_age(:,1), corr_Vgain_age(:,2)] = corr(age, Vgains, 'type', 'Pearson');
% sprintf('Correraliton between prosaccade maximum speeds and age \n\n \tFirst collum:  linear correlation coefficients\n \tSecond collum: p-values for testing the hypothesis of no correlation against the alternative that there is a nonzero correlation\n')
% disp(corr_Vmax_age)
tab_gain_age = table(corr_Vgain_age(:,1),corr_Vgain_age(:,2));
tab_gain_age.Properties.VariableNames = {'corr_koef','p_value'};
tab_gain_age.Properties.RowNames = {'p_gain_r', 'p_gain_l', 'p_gain_u'...
    'p_gain_d'};
