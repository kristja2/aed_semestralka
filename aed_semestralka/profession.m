clc, clear, close all

%% Data loadnig and fragmentation
tab = readtable('Data.xls');

corr_vect = [];
means = [];
stds = [];
for i = 1:8
        if sum(tab.prof == i) ~= 0
            % Corrs
            a = corr(tab.ye(tab.prof == i), tab.p_latency_r(tab.prof == i));
            corr_vect = [corr_vect a];
            
            % means and stds
            means = tab.p_latency_r(tab.prof == i);
        end
end
aoctool(tab.ye,tab.p_latency_l,tab.prof)

%% Hello anova1
anova1(tab.p_av_speed_u,tab.prof)
