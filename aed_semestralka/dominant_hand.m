clc, clear, close all

%% Data loading and fragmentation
tab = readtable('Data.xls');

righters_age = tab.ye(tab.rh_lh == 1);

rh_rp = tab.p_latency_r(tab.rh_lh == 1);  % Right hand, right prosaccades
rh_lp = tab.p_latency_l(tab.rh_lh == 1);  % Left hand, right prosaccades
rh_ra = tab.a_latency_r(tab.rh_lh ==1);  % Right hand, right antisaccade
rh_la = tab.a_latency_l(tab.rh_lh ==1);  % Right hand, left antisaccade
rh_lm = tab.m_latency_l(tab.rh_lh ==1);  % Right hand, left mixed
rh_rm = tab.m_latency_r(tab.rh_lh ==1);  % Right hand, right mixed

lh_lp = tab.p_latency_l(tab.rh_lh == 2);  % Left hand, left prosaccades
lh_rp = tab.p_latency_r(tab.rh_lh == 2);  % Left hand, right prosaccades


%% Fitting Gaussians
x_p = 50:5:350;
x_a = 50:5:500;

% Prosaccades
rh_rp_mean = mean(rh_rp); lh_rp_mean = mean(lh_rp);
rh_rp_std = std(rh_rp);lh_rp_std = std(lh_rp);
rh_lp_mean = mean(rh_lp); lh_lp_mean = mean(lh_lp);
rh_lp_std = std(rh_lp); lh_lp_std = std(lh_lp);

r_gauss = normpdf(x_p,rh_rp_mean,rh_rp_std);
l_gauss = normpdf(x_p,lh_rp_mean,lh_rp_std);
r_gauss_l = normpdf(x_p, rh_lp_mean, rh_lp_std);
l_gauss_l = normpdf(x_p, lh_lp_mean, lh_lp_std);

% Antisaccade
rh_ra_mean = mean(rh_ra); rh_la_mean = mean(rh_la);
rh_ra_std = std(rh_ra); rh_la_std = std(rh_la);

rh_ra_gauss = normpdf(x_a, rh_ra_mean, rh_ra_std);
rh_la_gauss = normpdf(x_a, rh_la_mean, rh_la_std);

% Mixed
rh_rm_mean = mean(rh_rm); rh_lm_mean = mean(rh_lm);
rh_rm_std = std(rh_rm); rh_lm_std = std(rh_lm);

rh_rm_gauss = normpdf(x_a, rh_rm_mean, rh_rm_std);
rh_lm_gauss = normpdf(x_a, rh_lm_mean, rh_lm_std);
%% Namaluj
figure
plot(x_p, r_gauss, '-ob', x_p, l_gauss, '-or', x_p, r_gauss_l, '-xb', x_p, l_gauss_l, '-xr')

% Vystup, je pouze 6 levaku!! Dost pochybuji, ze to ma nejakou vypovidajici
% vlastnost, proto, bych hypotezu definoval tak, ze jeestli se pravaci
% rychlejc kouknou doprava.. :D

figure
plot(x_p, r_gauss, x_p, r_gauss_l)
legend('right', 'left')
title('Right hand dominant, latency, prosaccade')

figure
plot(x_a, rh_ra_gauss, x_a, rh_la_gauss)
legend('right', 'left')
title('Right hand dominant, latency, antisaccade')

figure
plot(x_a, rh_rm_gauss, x_a, rh_lm_gauss)
legend('right', 'left')
title('Right hand dominant, latency, mixed')

%% Bude to lip korelovat?

corr_lat_rp = corr(righters_age,rh_rp);
corr_lat_lp = corr(righters_age,rh_lp);

corr_lat_ra = corr(righters_age,rh_ra);
corr_lat_la = corr(righters_age,rh_la);





