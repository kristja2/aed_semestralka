clc, clear, close all

%%
tab = readtable('Data.xls');
f_lp = tab.p_latency_l(tab.m_f == 2);

m_lp = tab.p_latency_l(tab.m_f == 1);

f_lp_mean = mean(f_lp);
m_lp_mean = mean(m_lp);
f_lp_std = std(f_lp);
m_lp_std = std(m_lp);

x = 100:300;
f_lp_gauss = normpdf(x, f_lp_mean, f_lp_std);
m_lp_gauss = normpdf(x, m_lp_mean, m_lp_std);

plot(x, f_lp_gauss, x, m_lp_gauss)